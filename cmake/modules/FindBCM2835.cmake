# This module tries to find libserialxx library and include files
#
# LIBBCM2835_INCLUDE_DIR, path where to find libwebsockets.h
# LIBBCM2835_LIBRARY_DIR, path where to find libwebsockets.so
# LIBBCM2835_LIBRARIES, the library to link against
# LIBBCM2835_FOUND, If false, do not try to use libWebSockets
#
# This currently works probably only for Linux

FIND_PATH ( LIBBCM2835_INCLUDE_DIR bcm2835.h
    /usr/local/include
    /usr/include
)

FIND_LIBRARY ( LIBBCM2835_LIBRARIES bcm2835
    /usr/local/lib
    /usr/lib
)

GET_FILENAME_COMPONENT( LIBBCM2835_LIBRARY_DIR ${LIBBCM2835_LIBRARIES} PATH )

SET ( LIBBCM2835_FOUND "NO" )
IF ( LIBBCM2835_INCLUDE_DIR )
    IF ( LIBBCM2835_LIBRARIES )
        SET ( LIBBCM2835_FOUND "YES" )
    ENDIF ( LIBBCM2835_LIBRARIES )
ENDIF ( LIBBCM2835_INCLUDE_DIR )

MARK_AS_ADVANCED(
    LIBBCM2835_LIBRARY_DIR
    LIBBCM2835_INCLUDE_DIR
    LIBBCM2835_LIBRARIES
)
