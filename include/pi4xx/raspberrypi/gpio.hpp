#ifndef PI4XX_RPI_GPIO_HPP
#define PI4XX_RPI_GPIO_HPP

#include <cstdint>
#include <pi4xx/i_gpio.hpp>

namespace pi4xx {
namespace raspberrypi {

    class GPIO : public IGPIO
    {
      public:
        /**
         *
         */
        GPIO(NumberingScheme scheme = NumberingScheme::SOC_NUMBERING);

        /**
         *
         */
        ~GPIO();

        /**
         * Set pin mode.
         *
         * @param pin_number Pin number
         * @param mode Pin mode @see PinMode
         */
        void setPinMode(uint8_t pin_number, PinMode mode) override;

        /**
         * Set pi pull resistore state.
         *
         * @param pin_number Pin number
         */
        void setPullResistor(uint8_t pin_number, PullResistor pud) override;

        /**
         * Write digital value to an output pin.
         *
         * @param pin_number Pin number
         * @param value Digital value to be written
         */
        void setOutput(uint8_t pin_number, int value) override;

        /**
         * Read digital value from an input pin.
         *
         * @param pin_number Pin number
         */
        int getInput(uint8_t pin_number) override;

        // void pwmWrite(uint8_t pin_number, int value);
        // int analogRead(uint8_t pin_number);
        // void analogWrite(uint8_t pin_number, int value);

      private:
        NumberingScheme m_scheme  = NumberingScheme::SOC_NUMBERING;
        bool m_layout_v1          = false;
        unsigned m_max_pin_number = 0;
        int* m_pin_mapping        = nullptr;

        int remapPinNumber(uint8_t pin_number);

        unsigned int m_gpio;
        unsigned int m_pwm;
        unsigned int m_clk;
        unsigned int m_pads;
        unsigned int m_timer;

        volatile unsigned int* mm_gpio;
        volatile unsigned int* mm_pwm;
        volatile unsigned int* mm_clk;
        volatile unsigned int* mm_pads;
        volatile unsigned int* mm_timer;

        // static volatile unsigned int* mm_timerIrqRaw;
    };

} // namespace raspberrypi
} // namespace pi4xx

#endif /* PI4XX_RPI_GPIO_HPP */