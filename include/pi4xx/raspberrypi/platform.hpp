#ifndef PI4XX_RASPBERRY_PI_HPP
#define PI4XX_RASPBERRY_PI_HPP

namespace pi4xx {
namespace raspberrypi {

    enum class Type
    {
        TYPE_A        = 0x00,
        TYPE_B        = 0x01,
        TYPE_A_PLUS   = 0x02,
        TYPE_B_PLUS   = 0x03,
        TYPE_2B       = 0x04,
        TYPE_ALPHA    = 0x05,
        TYPE_CM1      = 0x06,
        TYPE_07       = 0x07, // unused
        TYPE_3B       = 0x08,
        TYPE_ZERO     = 0x09,
        TYPE_CM3      = 0x0a,
        TYPE_0b       = 0x0b, // unused
        TYPE_ZERO_W   = 0x0c,
        TYPE_3B_PLUS  = 0x0d,
        TYPE_3A_PLUS  = 0x0e,
        TYPE_0f       = 0x0f,
        TYPE_CM3_PLUS = 0x10,
        TYPE_4B       = 0x11,
        UNKNOWN
    };

    enum class RamMemorySize
    {
        RAM_256MB = 0,
        RAM_512MB,
        RAM_1GB,
        RAM_2GB,
        RAM_4GB,
        UNKNOWN
    };

    enum class Manufacturer
    {
        SONY_UK,
        EGOMAN,
        EMBEST_1,
        SONY_JAPAN,
        EMBEST_2,
        STADIUM,
        UNKNOWN
    };

    enum Processor
    {
        BCM2835 = 0,
        BCM2836,
        BCM2837,
        BCM2711,
        UNKNOWN
    };

    struct Revision
    {
        bool new_style            = false;
        RamMemorySize memory_size = RamMemorySize::RAM_256MB;
        Manufacturer manufacturer = Manufacturer::SONY_UK;
        Processor processor       = Processor::BCM2835;
        Type type                 = Type::TYPE_A;
        unsigned revision         = 0;
    };

    Revision readRevision();

} // namespace raspberrypi
} // namespace pi4xx

#endif /* PI4XX_RASPBERRY_PI_HPP */