#ifndef PI4XX_IGPIO_HPP
#define PI4XX_IGPIO_HPP

#include <cstdint>
#include <pi4xx/common.hpp>

namespace pi4xx {

class IGPIO
{
  public:
    /**
     * Set pin mode.
     *
     * @param pin_number Pin number
     * @param mode Pin mode @see PinMode
     */
    virtual void setPinMode(uint8_t pin_number, PinMode mode) = 0;

    /**
     * Set pi pull resistore state.
     *
     * @param pin_number Pin number
     */
    virtual void setPullResistor(uint8_t pin_number, PullResistor pud) = 0;

    /**
     * Write digital value to an output pin.
     *
     * @param pin_number Pin number
     * @param value Digital value to be written
     */
    virtual void setOutput(uint8_t pin_number, int value) = 0;

    /**
     * Read digital value from an input pin.
     *
     * @param pin_number Pin number
     */
    virtual int getInput(uint8_t pin_number) = 0;

    // void pwmWrite(uint8_t pin_number, int value);
    // int analogRead(uint8_t pin_number);
    // void analogWrite(uint8_t pin_number, int value);
};

} // namespace pi4xx

#endif /* PI4XX_IGPIO_HPP */