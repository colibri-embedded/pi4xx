#ifndef PI4XX_COMMON_HPP
#define PI4XX_COMMON_HPP

namespace pi4xx {

enum class NumberingScheme
{
    BOARD_NUMBERING,
    SOC_NUMBERING,
    // WIRINGPI_NUMBERING
};

enum class EdgeType
{
    NONE,
    BOTH,
    RISING,
    FALLING
};

enum class PinMode
{
    INPUT = 0,
    OUTPUT,
    // PWM_OUTPUT,
    // ANALOG_INPUT,
    // ANALOG_OUTPUT,
    // GPIO_CLOCK,
    // SOFT_PWM_OUTPUT,
    // SOFT_TONE_OUTPUT,
    // PWM_TONE_OUTPUT,
};

enum class PullResistor
{
    OFF       = 0,
    PULL_DOWN = 1,
    PULL_UP   = 2
};

} // namespace pi4xx

#endif /* PI4XX_COMMON_HPP */
