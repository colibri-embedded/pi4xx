#include <iostream>
#include <pi4xx/raspberrypi/platform.hpp>
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <string>

using namespace pi4xx::raspberrypi;

template<typename T>
bool from_hex(const std::string& hexValue, T& result)
{
    std::stringstream ss;
    ss << std::hex << hexValue;
    ss >> result;

    return !ss.fail();
}

std::string exec(const char* cmd)
{
    char buffer[128];
    std::string result = "";
    FILE* pipe         = popen(cmd, "r");
    if (!pipe)
        throw std::runtime_error("popen() failed!");
    try {
        while (fgets(buffer, sizeof buffer, pipe) != NULL) {
            result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

Revision pi4xx::raspberrypi::readRevision()
{
    Revision data;

    try {
        auto hex = exec("cat /proc/cpuinfo | grep Revision | awk '{print $3}'");
        unsigned revision;
        if (!from_hex(hex, revision))
            throw std::runtime_error("revision hex number is invalid: \"" + hex + "\"");

        // uuuu|uuuu FMMM|CCCC PPPP|TTTT TTTT|RRRR
        if (revision & 0x00800000) {
            data.new_style = true;
            // Revision
            data.revision = (revision & 0x0f);
            // Type
            auto type = (revision >> 4) & 0xff;
            if (type < (unsigned)Type::UNKNOWN)
                data.type = (Type)type;
            else
                throw std::runtime_error("unknown type code \"" + std::to_string(type) + "\"");
            // Processor
            auto proc = (revision >> 12) & 0x0f;
            if (proc < (unsigned)Processor::UNKNOWN)
                data.processor = (Processor)proc;
            else
                throw std::runtime_error("unknown processor code \"" + std::to_string(proc) + "\"");

            // Manufacturer...Who cares xD

            // Memory size
            auto mem = (revision >> 16) & 0x07;
            if (mem < (unsigned)RamMemorySize::UNKNOWN)
                data.memory_size = (RamMemorySize)mem;
            else
                throw std::runtime_error("unknown memory code \"" + std::to_string(proc) + "\"");

        } else {
            data.new_style = false;
            switch (revision) {
                case 0x0002:
                case 0x0003:
                    data.type        = Type::TYPE_B;
                    data.memory_size = RamMemorySize::RAM_256MB;
                    data.revision    = 10;
                    break;
                case 0x0004:
                case 0x0005:
                case 0x0006:
                    data.type        = Type::TYPE_B;
                    data.memory_size = RamMemorySize::RAM_256MB;
                    data.revision    = 20;
                    break;
                case 0x0007:
                case 0x0008:
                case 0x0009:
                    data.type        = Type::TYPE_A;
                    data.memory_size = RamMemorySize::RAM_256MB;
                    data.revision    = 20;
                    break;
                case 0x000d:
                case 0x000e:
                case 0x000f:
                    data.type        = Type::TYPE_B;
                    data.memory_size = RamMemorySize::RAM_512MB;
                    data.revision    = 20;
                    break;
                case 0x0012:
                    data.type        = Type::TYPE_A_PLUS;
                    data.memory_size = RamMemorySize::RAM_256MB;
                    data.revision    = 11;
                    break;
                case 0x0010:
                case 0x0013:
                    data.type        = Type::TYPE_B_PLUS;
                    data.memory_size = RamMemorySize::RAM_512MB;
                    data.revision    = 12;
                    break;
                case 0x0011:
                case 0x0014:
                    data.type        = Type::TYPE_CM1;
                    data.memory_size = RamMemorySize::RAM_512MB;
                    data.revision    = 10;
                    break;
                case 0x0015:
                    data.type        = Type::TYPE_A_PLUS;
                    data.memory_size = RamMemorySize::RAM_256MB;
                    data.revision    = 11;
                    break;
                default:
                    throw std::runtime_error("unknown old-style revision");
            }
        }

    } catch (...) {
        throw std::runtime_error("cannot read board revision");
    }

    return data;
}