#include <bcm2835.h>
#include <fcntl.h>
#include <pi4xx/gpio.hpp>
#include <pi4xx/raspberrypi/platform.hpp>
#include <stdexcept>
#include <sys/mman.h>

using namespace pi4xx::raspberrypi;

// static int boardV1Mapping[] = {
//     /*01*/ -1, /*02*/ -1,
//     /*03*/ 0,  /*04*/ -1,
//     /*05*/ 1,  /*06*/ -1,
//     /*07*/ 4,  /*08*/ 14,
//     /*09*/ -1, /*10*/ 15,
//     /*11*/ 17, /*12*/ 18,
//     /*13*/ 21, /*14*/ -1,
//     /*15*/ 22, /*16*/ 23,
//     /*17*/ -1, /*18*/ 24,
//     /*19*/ 10, /*20*/ -1,
//     /*21*/ 9,  /*22*/ 25,
//     /*23*/ 11, /*24*/ 8,
//     /*25*/ -1, /*26*/ 7,
// };

// static int boardV2Mapping[] = {
//     /*01*/ -1, /*02*/ -1,
//     /*03*/ 2,  /*04*/ -1,
//     /*05*/ 3,  /*06*/ -1,
//     /*07*/ 4,  /*08*/ 14,
//     /*09*/ -1, /*10*/ 15,
//     /*11*/ 17, /*12*/ 18,
//     /*13*/ 27, /*14*/ -1,
//     /*15*/ 22, /*16*/ 23,
//     /*17*/ -1, /*18*/ 24,
//     /*19*/ 10, /*20*/ -1,
//     /*21*/ 9,  /*22*/ 25,
//     /*23*/ 11, /*24*/ 8,
//     /*25*/ -1, /*26*/ 7,
//     /*27*/ 0,  /*28*/ 1,
//     /*29*/ 5,  /*30*/ -1,
//     /*31*/ 6,  /*32*/ 12,
//     /*33*/ 13, /*34*/ -1,
//     /*35*/ 19, /*36*/ 16,
//     /*37*/ 26, /*38*/ 20,
//     /*39*/ -1, /*40*/ 21
// };

static int boardV1Mapping[] = {
    /*01*/ -1,
    /*02*/ -1,
    /*03*/ RPI_GPIO_P1_03,
    /*04*/ -1,
    /*05*/ RPI_GPIO_P1_05,
    /*06*/ -1,
    /*07*/ RPI_GPIO_P1_07,
    /*08*/ RPI_GPIO_P1_08,
    /*09*/ -1,
    /*10*/ RPI_GPIO_P1_10,
    /*11*/ RPI_GPIO_P1_11,
    /*12*/ RPI_GPIO_P1_12,
    /*13*/ RPI_GPIO_P1_13,
    /*14*/ -1,
    /*15*/ RPI_GPIO_P1_15,
    /*16*/ RPI_GPIO_P1_16,
    /*17*/ -1,
    /*18*/ RPI_GPIO_P1_18,
    /*19*/ RPI_GPIO_P1_19,
    /*20*/ -1,
    /*21*/ RPI_GPIO_P1_21,
    /*22*/ RPI_GPIO_P1_22,
    /*23*/ RPI_GPIO_P1_23,
    /*24*/ RPI_GPIO_P1_24,
    /*25*/ -1,
    /*26*/ RPI_GPIO_P1_26,
};

static int boardV2Mapping[] = {
    /*01*/ -1,
    /*02*/ -1,
    /*03*/ RPI_V2_GPIO_P1_03,
    /*04*/ -1,
    /*05*/ RPI_V2_GPIO_P1_05,
    /*06*/ -1,
    /*07*/ RPI_V2_GPIO_P1_07,
    /*08*/ RPI_V2_GPIO_P1_08,
    /*09*/ -1,
    /*10*/ RPI_V2_GPIO_P1_10,
    /*11*/ RPI_V2_GPIO_P1_11,
    /*12*/ RPI_V2_GPIO_P1_12,
    /*13*/ RPI_V2_GPIO_P1_13,
    /*14*/ -1,
    /*15*/ RPI_V2_GPIO_P1_15,
    /*16*/ RPI_V2_GPIO_P1_16,
    /*17*/ -1,
    /*18*/ RPI_V2_GPIO_P1_18,
    /*19*/ RPI_V2_GPIO_P1_19,
    /*20*/ -1,
    /*21*/ RPI_V2_GPIO_P1_21,
    /*22*/ RPI_V2_GPIO_P1_22,
    /*23*/ RPI_V2_GPIO_P1_23,
    /*24*/ RPI_V2_GPIO_P1_24,
    /*25*/ -1,
    /*26*/ RPI_V2_GPIO_P1_26,
    /*27*/ -1,
    /*28*/ -1,
    /*29*/ RPI_V2_GPIO_P1_29,
    /*30*/ -1,
    /*31*/ RPI_V2_GPIO_P1_31,
    /*32*/ RPI_V2_GPIO_P1_32,
    /*33*/ RPI_V2_GPIO_P1_33,
    /*34*/ -1,
    /*35*/ RPI_V2_GPIO_P1_35,
    /*36*/ RPI_V2_GPIO_P1_36,
    /*37*/ RPI_V2_GPIO_P1_37,
    /*38*/ RPI_V2_GPIO_P1_38,
    /*39*/ -1,
    /*40*/ RPI_V2_GPIO_P1_40
};

// static int wiringPiV2Mapping[] = {
//     /*00*/ 17, /*01*/ 18,
//     /*02*/ 27, /*03*/ 22,
//     /*04*/ 23, /*05*/ 24,
//     /*06*/ 25, /*07*/ 4,
//     /*08*/ 2,  /*09*/ 3,
//     /*10*/ 8,  /*11*/ 7,
//     /*12*/ 10, /*13*/ 9,
//     /*14*/ 11, /*15*/ 14,
//     /*16*/ 15, /*17*/ -1,
//     /*18*/ -1, /*19*/ -1,
//     /*20*/ -1, /*21*/ 5,
//     /*22*/ 6,  /*23*/ 13,
//     /*24*/ 19, /*25*/ 26,
//     /*26*/ 12, /*27*/ 16,
//     /*28*/ 20, /*29*/ 21,
//     /*30*/ 0,  /*31*/ 1,
// };

GPIO::GPIO(NumberingScheme scheme)
  : m_scheme(scheme)
{
    auto board = readRevision();

    if ((board.type == Type::TYPE_A || board.type == Type::TYPE_B) && board.revision == 10) {
        // layout v1
        m_layout_v1      = true;
        m_pin_mapping    = boardV1Mapping;
        m_max_pin_number = 26;
    } else {
        // layout v2
        m_layout_v1      = false;
        m_pin_mapping    = boardV2Mapping;
        m_max_pin_number = 40;
    }

    bcm2835_init();
}

/**
 *
 */
GPIO::~GPIO()
{
    bcm2835_close();
}

int GPIO::remapPinNumber(uint8_t pin_number)
{
    switch (m_scheme) {
        case NumberingScheme::SOC_NUMBERING: {
            if (pin_number > 31)
                return -1;
            return pin_number;
        } break;
        case NumberingScheme::BOARD_NUMBERING: {
            if (pin_number > m_max_pin_number || pin_number < 1)
                return -1;
            return m_pin_mapping[pin_number - 1];
        } break;
    }
    return -1;
}

/**
 * Set pin mode.
 *
 * @param pin_number Pin number
 * @param mode Pin mode @see PinMode
 */
void GPIO::setPinMode(uint8_t pin_number, PinMode mode)
{
    //
    int pin = remapPinNumber(pin_number);
    if (pin == -1)
        throw std::invalid_argument("Pin with number " + std::to_string(pin_number) + " is not supported");

    switch (mode) {
        case PinMode::INPUT:
            bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);
            break;
        case PinMode::OUTPUT:
            bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);
            break;
        default:
            throw std::invalid_argument("Mode is not supported");
    }
}

/**
 * Set pi pull resistore state.
 *
 * @param pin_number Pin number
 */
void GPIO::setPullResistor(uint8_t pin_number, PullResistor pud)
{
    int pin = remapPinNumber(pin_number);
    if (pin == -1)
        throw std::invalid_argument("Pin with number " + std::to_string(pin_number) + " is not supported");

    bcm2835_gpio_set_pud(pin, (uint8_t)pud);
}

/**
 * Write digital value to an output pin.
 *
 * @param pin_number Pin number
 * @param value Digital value to be written
 */
void GPIO::setOutput(uint8_t pin_number, int value)
{
    int pin = remapPinNumber(pin_number);
    if (pin == -1)
        throw std::invalid_argument("Pin with number " + std::to_string(pin_number) + " is not supported");

    if (value)
        bcm2835_gpio_set(pin);
    else
        bcm2835_gpio_clr(pin);
}

/**
 * Read digital value from an input pin.
 *
 * @param pin_number Pin number
 */
int GPIO::getInput(uint8_t pin_number)
{
    int pin = remapPinNumber(pin_number);
    if (pin == -1)
        throw std::invalid_argument("Pin with number " + std::to_string(pin_number) + " is not supported");

    return bcm2835_gpio_lev(pin);
}