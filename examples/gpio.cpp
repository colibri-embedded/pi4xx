#include <chrono>
#include <iostream>
#include <memory>
#include <pi4xx/gpio.hpp>
#include <thread>

using namespace pi4xx;
using namespace pi4xx::raspberrypi;

int main()
{
    auto gpio = GPIO(NumberingScheme::SOC_NUMBERING);
    auto pin  = 0;

    // gpio.setPinMode(pin, PinMode::OUTPUT);
    // gpio.setOutput(pin, true);
    // std::this_thread::sleep_for(std::chrono::seconds(1));
    // gpio.setOutput(pin, false);
    // std::this_thread::sleep_for(std::chrono::seconds(1));
    // gpio.setOutput(pin, true);

    gpio.setPinMode(pin, PinMode::INPUT);
    gpio.setPullResistor(pin, PullResistor::PULL_DOWN);

    while (1) {
        auto value = gpio.getInput(pin);
        std::cout << "Value = " << value << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    return 0;
}